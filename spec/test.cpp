#define CATCH_CONFIG_MAIN
#include <iostream>
#include <ctime>
#include "catch.hpp"
#include "../warpp.hpp"

using namespace std;

TEST_CASE( "Creating a new Moment object and calling toString() on it will return the current (local) datetime in YYYY-MM-DD HH:MM:SS format" )
{

  Moment* mm = new Moment();

  time_t t;
  struct tm * tinf;
  char buffer[20];
  time(&t);
  tinf = localtime(&t);
  mktime(tinf);
  strftime(buffer,20,"%F %H:%M:%S",tinf);

  string cresult(buffer);

  REQUIRE ( mm->toString().compare(cresult) == 0 );

}

TEST_CASE( "It should be possible to set a Moment object with a string in YYYY-MM-DD HH:MM:SS format" )
{

  string starter("2017-01-01 23:00:00");
  Moment * mm = new Moment(starter);

  REQUIRE ( mm->toString().compare(starter) == 0 );

  delete mm;

}

TEST_CASE( "It should be possible to initialize one Moment object with another" )
{

  Moment mm("2016-08-21 10:30:00");
  Moment other(mm);

  REQUIRE( mm == other );
  REQUIRE( mm.toString().compare(other.toString()) == 0 );

}

TEST_CASE( "It should be possible to reset a time by passing in a new string" )
{
  string starter("2017-02-05 16:22:35");
  Moment * mm = new Moment(starter);

  REQUIRE ( mm->toString().compare(starter) == 0 );

  mm->setstring("2017-12-30 05:22:40");

  REQUIRE ( mm->toString().compare("2017-12-30 05:22:40") == 0 );

  delete mm;

}

TEST_CASE( "It should be possible to reset a time by passing in another Moment object" )
{
  string starter("2017-02-05 16:22:35");
  string otherstarter("2017-12-30 05:22:40");
  Moment * mm = new Moment(starter);
  Moment * other = new Moment(otherstarter);

  mm->setmoment(*other);
  REQUIRE ( mm->toString().compare("2017-12-30 05:22:40") == 0 );

  delete mm;

}

TEST_CASE( "It should be possible to add to a time" )
{
  string starter("2017-02-05 16:22:35");
  Moment * mm = new Moment(starter);

  mm->add(61, "second");
  REQUIRE ( mm->toString().compare("2017-02-05 16:23:36") == 0 );

  mm->add(123, "second");
  REQUIRE ( mm->toString().compare("2017-02-05 16:25:39") == 0 );

  mm->add(86401, "second");
  REQUIRE ( mm->toString().compare("2017-02-06 16:25:40") == 0 );

  mm->add(61, "minute");
  REQUIRE ( mm->toString().compare("2017-02-06 17:26:40") == 0 );

  mm->add(10080, "minute");
  REQUIRE ( mm->toString().compare("2017-02-13 17:26:40") == 0 );

  mm->add(525600, "minute");
  REQUIRE ( mm->toString().compare("2018-02-13 17:26:40") == 0 );

  mm->add(1, "hour");
  REQUIRE ( mm->toString().compare("2018-02-13 18:26:40") == 0 );

  mm->add(26, "hour");
  REQUIRE ( mm->toString().compare("2018-02-14 20:26:40") == 0 );

  mm->add(360, "hour");
  REQUIRE ( mm->toString().compare("2018-03-01 20:26:40") == 0 );

  mm->add(365, "day");
  REQUIRE ( mm->toString().compare("2019-03-01 20:26:40") == 0 );

  mm->add(1, "day");
  REQUIRE ( mm->toString().compare("2019-03-02 20:26:40") == 0 );

  mm->add(1, "year");
  REQUIRE ( mm->toString().compare("2020-03-02 20:26:40") == 0 );

  mm->add(21, "year");
  REQUIRE ( mm->toString().compare("2041-03-02 20:26:40") == 0 );

  delete mm;

}

TEST_CASE( "It should be possible to subtract from a time" )
{

  string starter("2017-02-05 16:22:35");
  Moment * mm = new Moment(starter);

  mm->subtract(61, "second");
  REQUIRE ( mm->toString().compare("2017-02-05 16:21:34") == 0 );

  mm->subtract(123, "second");
  REQUIRE ( mm->toString().compare("2017-02-05 16:19:31") == 0 );

  mm->subtract(86401, "second");
  REQUIRE ( mm->toString().compare("2017-02-04 16:19:30") == 0 );

  mm->subtract(61, "minute");
  REQUIRE ( mm->toString().compare("2017-02-04 15:18:30") == 0 );

  mm->subtract(10080, "minute");
  REQUIRE ( mm->toString().compare("2017-01-28 15:18:30") == 0 );

  mm->subtract(525600, "minute");
  REQUIRE ( mm->toString().compare("2016-01-29 15:18:30") == 0 );

  mm->subtract(1, "hour");
  REQUIRE ( mm->toString().compare("2016-01-29 14:18:30") == 0 );

  mm->subtract(26, "hour");
  REQUIRE ( mm->toString().compare("2016-01-28 12:18:30") == 0 );

  mm->subtract(360, "hour");
  REQUIRE ( mm->toString().compare("2016-01-13 12:18:30") == 0 );

  mm->subtract(365, "day");
  REQUIRE ( mm->toString().compare("2015-01-13 12:18:30") == 0 );

  mm->subtract(1, "day");
  REQUIRE ( mm->toString().compare("2015-01-12 12:18:30") == 0 );

  mm->subtract(21, "year");
  REQUIRE ( mm->toString().compare("1994-01-12 12:18:30") == 0 );

  delete mm;

}

TEST_CASE( "It should be possible to compare two Moment objects" )
{

  Moment * mearly = new Moment("2016-02-01 23:00:00");
  Moment * mlate = new Moment("2016-02-28 23:00:00");
  Moment * mmed = new Moment("2016-02-14 23:00:00");
  Moment * dup = new Moment("2016-02-14 23:00:00");

  REQUIRE( mearly->isBefore(*mlate) );
  REQUIRE( mlate->isAfter(*mearly) );
  REQUIRE( mmed->isAfter(*mearly) );
  REQUIRE( *mmed > *mearly );
  REQUIRE( mmed->isBefore(*mlate) );
  REQUIRE( mmed->isSame(*dup) );

  delete mearly;
  delete mlate;
  delete mmed;
  delete dup;

}

TEST_CASE( "The overloaded operators stand in for isBefore, isAfter and isSame" )
{
  Moment mearly("2016-02-01 23:00:00");
  Moment mlate("2016-02-28 23:00:00");
  Moment mmed("2016-02-14 23:00:00");
  Moment dup("2016-02-14 23:00:00");

  REQUIRE( mearly < mlate );
  REQUIRE( (mearly > mlate) == false );
  REQUIRE( mearly != mlate );
  REQUIRE( mlate > mearly );
  REQUIRE( (mlate < mearly) == false );
  REQUIRE( mmed > mearly );
  REQUIRE( mmed >= mearly );
  REQUIRE( mmed != mearly );
  REQUIRE( mmed < mlate );
  REQUIRE( mmed <= mlate );
  REQUIRE( mmed == dup );
  REQUIRE( (mmed != dup) == false );
  REQUIRE( mmed <= dup );
  REQUIRE( mmed >= dup );

}

TEST_CASE( "A Span should take two strings and intialize two Moment objects with them, representing start and end" )
{

  Span sp("2016-01-01 00:00:00", "2016-01-01 01:00:00");
  REQUIRE( sp.toString().compare("2016-01-01 00:00:00 - 2016-01-01 01:00:00") == 0 );

}

TEST_CASE( "A Span should be able to return references to its component parts" )
{

  Span sp("2016-01-01 00:00:00", "2016-01-01 01:00:00");
  Moment st = sp.getStart();
  Moment en = sp.getEnd();

  REQUIRE( st.toString().compare("2016-01-01 00:00:00") == 0 );
  REQUIRE( en.toString().compare("2016-01-01 01:00:00") == 0 );
  REQUIRE( st.toString().compare("2016-01-01 01:00:00") != 0 );
  REQUIRE( st < en );
  REQUIRE( (st > en) == false );
  REQUIRE( st != en );

}

TEST_CASE( "A Span should be able to do comparison tests with other spans" )
{

  Span sp_one("2016-01-01 00:00:00", "2016-01-01 01:00:00");
  Span sp_two("2016-01-01 00:00:00", "2016-01-01 01:00:00");
  Span sp_three("2016-01-01 00:00:00", "2016-01-01 01:00:01");
  Span sp_smaller("2016-01-01 00:00:00", "2016-01-01 00:30:00");
  Span sp_larger("2015-12-31 00:00:00", "2016-01-31 00:00:00");

  REQUIRE( sp_one.isSame(sp_two) );
  REQUIRE( !sp_one.isSame(sp_three) );
  REQUIRE( !sp_two.isSame(sp_three) );
  REQUIRE( sp_three.isSame(sp_three) );

  REQUIRE( sp_larger.contains(sp_smaller) );
  REQUIRE( !sp_smaller.contains(sp_larger) );
  REQUIRE( sp_one.contains(sp_two) );
  REQUIRE( sp_two.contains(sp_one) );
  REQUIRE( !sp_two.contains(sp_three) );
  REQUIRE( sp_three.contains(sp_two) );

  REQUIRE( sp_larger.subsumes(sp_smaller) );
  REQUIRE( !sp_smaller.subsumes(sp_larger) );
  REQUIRE( !sp_three.subsumes(sp_two) );
  REQUIRE( !sp_one.subsumes(sp_two) );

}

TEST_CASE( "A Span overlaps another Span when it (i) starts after the other but before the other ends OR (ii) ends after the other starts but before it ends OR (iii) subsumes it" )
{

  Span sp_one("2016-05-01 08:00:00", "2016-05-01 10:00:00");
  Span sp_two("2016-05-01 09:00:00", "2016-05-01 11:00:00");
  Span sp_three("2016-05-01 07:00:00", "2016-05-01 09:00:00");
  Span sp_four("2016-05-01 07:00:00", "2016-05-01 11:00:00");
  Span sp_five("2016-05-01 09:10:00", "2016-05-01 09:30:00");
  Span sp_six("2016-05-01 08:00:00", "2016-05-01 10:00:00");
  Span sp_seven("2016-05-01 18:00:00", "2016-05-01 20:00:00");

  REQUIRE( sp_one.overlaps(sp_two) );
  REQUIRE( sp_two.overlaps(sp_one) );
  REQUIRE( sp_three.overlaps(sp_one) );
  REQUIRE( sp_one.overlaps(sp_three) );
  REQUIRE( !sp_one.overlaps(sp_seven) );
  REQUIRE( sp_four.overlaps(sp_one) );
  REQUIRE( sp_one.overlaps(sp_four) );
  REQUIRE( sp_five.overlaps(sp_one) );
  REQUIRE( sp_one.overlaps(sp_five) );
  REQUIRE( sp_one.overlaps(sp_six) );
  REQUIRE( sp_six.overlaps(sp_one) );
  REQUIRE( !sp_four.overlaps(sp_seven) );

}

TEST_CASE( "A range given a start, and end, a duration and a unit should create Spans of duration 'duration' at increments of that unit between the start and end" )
{
  string testref = R"(2016-10-05 08:00:00 - 2016-10-05 09:00:00
2016-10-05 09:00:00 - 2016-10-05 10:00:00
2016-10-05 10:00:00 - 2016-10-05 11:00:00
2016-10-05 11:00:00 - 2016-10-05 12:00:00
2016-10-05 12:00:00 - 2016-10-05 13:00:00
2016-10-05 13:00:00 - 2016-10-05 14:00:00
2016-10-05 14:00:00 - 2016-10-05 15:00:00
2016-10-05 15:00:00 - 2016-10-05 16:00:00
2016-10-05 16:00:00 - 2016-10-05 17:00:00
)";

  Range r("2016-10-05 08:00:00", "2016-10-05 17:00:00", 3600, "second");
  REQUIRE( r.toString().compare(testref) == 0 );

}

TEST_CASE( "A range given a start, and end, a duration, a skip and a unit should create Spans of duration 'duration' at increments of the value of 'skip' at that unit between the start and end" )
{
  string testref = R"(2016-10-05 08:00:00 - 2016-10-05 09:00:00
2016-10-05 08:20:00 - 2016-10-05 09:20:00
2016-10-05 08:40:00 - 2016-10-05 09:40:00
2016-10-05 09:00:00 - 2016-10-05 10:00:00
2016-10-05 09:20:00 - 2016-10-05 10:20:00
2016-10-05 09:40:00 - 2016-10-05 10:40:00
2016-10-05 10:00:00 - 2016-10-05 11:00:00
2016-10-05 10:20:00 - 2016-10-05 11:20:00
2016-10-05 10:40:00 - 2016-10-05 11:40:00
2016-10-05 11:00:00 - 2016-10-05 12:00:00
2016-10-05 11:20:00 - 2016-10-05 12:20:00
2016-10-05 11:40:00 - 2016-10-05 12:40:00
2016-10-05 12:00:00 - 2016-10-05 13:00:00
2016-10-05 12:20:00 - 2016-10-05 13:20:00
2016-10-05 12:40:00 - 2016-10-05 13:40:00
2016-10-05 13:00:00 - 2016-10-05 14:00:00
2016-10-05 13:20:00 - 2016-10-05 14:20:00
2016-10-05 13:40:00 - 2016-10-05 14:40:00
2016-10-05 14:00:00 - 2016-10-05 15:00:00
2016-10-05 14:20:00 - 2016-10-05 15:20:00
2016-10-05 14:40:00 - 2016-10-05 15:40:00
2016-10-05 15:00:00 - 2016-10-05 16:00:00
2016-10-05 15:20:00 - 2016-10-05 16:20:00
2016-10-05 15:40:00 - 2016-10-05 16:40:00
2016-10-05 16:00:00 - 2016-10-05 17:00:00
)";

  Range r("2016-10-05 08:00:00", "2016-10-05 17:00:00", 3600, 1200, "second");
  REQUIRE( r.toString().compare(testref) == 0 );
}

TEST_CASE( " It should be possible to access an object in range positionally" )
{

  Range r("2016-10-05 08:00:00", "2016-10-05 17:00:00", 3600, 1200, "second");
  Span* sp = r[3];

  REQUIRE( sp->toString().compare("2016-10-05 09:00:00 - 2016-10-05 10:00:00") == 0);

}

TEST_CASE( "Range should function like an iterator" )
{
  Range r("2016-10-05 08:00:00", "2016-10-05 17:00:00", 3600, 1200, "second");
  string container;
  Span* sp;
  ostringstream oss;

  REQUIRE( r.size() == 25 );

  while(sp = r.next())
  {
    oss << sp->toString() << endl;
  }

  REQUIRE( oss.str().compare(r.toString()) == 0 );
  REQUIRE( r.next()->toString().compare("2016-10-05 08:00:00 - 2016-10-05 09:00:00") == 0 );
  REQUIRE( r.next()->toString().compare("2016-10-05 08:20:00 - 2016-10-05 09:20:00") == 0 );
  r.reset();
  REQUIRE( r.next()->toString().compare("2016-10-05 08:00:00 - 2016-10-05 09:00:00") == 0 );
}
