#include <ctime>
#include <string>
#include <vector>
#include <cstring>
#include <cstdlib>
#include <sstream>
#include <iostream>

using std::string;
using std::vector;
using std::cout;
using std::endl;
using std::ostringstream;

class Moment {
  public:
    Moment(){ time(&t); tinf = localtime(&t); unpack(); fmt = toString();}
    Moment(string);
    Moment(Moment&);
    string toString(void);
    bool isBefore(Moment&);
    bool isAfter(Moment&);
    bool isSame(Moment&);
    void add(int, string);
    void subtract(int, string);
    void setstring(string);
    void setmoment(Moment&);
    void display(void);
    time_t getTime(void){ return timegm(tinf); }
    bool operator<(Moment&);
    bool operator<=(Moment&);
    bool operator>(Moment&);
    bool operator>=(Moment&);
    bool operator==(Moment&);
    bool operator!=(Moment&);
  private:
    int year;
    int month;
    int day;
    int hour;
    int minute;
    int second;
    time_t t;
    struct tm * tinf;
    void adjust(void);
    void unpack(void);
    string fmt;
};

Moment::Moment(string inputstring)
{
  time(&t);
  tinf = localtime(&t);
  setstring(inputstring);
}

Moment::Moment(Moment& other)
{
  time(&t);
  tinf = localtime(&t);
  setstring(other.toString());
}

void Moment::display(void)
{
  cout << "YEAR: " << year << endl;
  cout << "MONTH: " << month << endl;
  cout << "DAY: " << day << endl;
  cout << "HOUR: " << hour << endl;
  cout << "MINUTE: " << minute << endl;
  cout << "SECOND: " << second << endl;
  cout << endl << endl;
}

void Moment::unpack(void)
{
  timegm(tinf);
  char buffer[20];
  strftime(buffer,20,"%F %H:%M:%S",tinf);
  string container(buffer);
  setstring(container);
}

void Moment::adjust(void)
{
  tinf->tm_year = year - 1900;
  tinf->tm_mon = month - 1;
  tinf->tm_mday = day;
  tinf->tm_hour = hour;
  tinf->tm_min = minute;
  tinf->tm_sec = second;
  //cout << year << "-" << month << "-" << day << " " << hour << ":" << minute << ":" << second << endl << endl;
}

string Moment::toString(void)
{
  adjust();
  timegm(tinf);
  char buffer[20];
  strftime(buffer,20,"%F %H:%M:%S",tinf);
  string container(buffer);
  return container;
}

void Moment::setstring(string inputstring)
{
  char* pch;
  char* pend;

  fmt = inputstring;

  vector<char> v(fmt.size()+1);
  memcpy( &v.front(), fmt.c_str(), fmt.size()+1 );
  vector<int> holder;
  pch = strtok(v.data(), " :-");
  while(pch != NULL)
  {
    holder.push_back(strtol(pch, &pend, 10));
    pch = strtok(NULL, " -:");
  }
  year = holder[0];
  month = holder[1];
  day = holder[2];
  hour = holder[3];
  minute = holder[4];
  second = holder[5];
  adjust();
}

void Moment::setmoment(Moment& other)
{
  setstring(other.toString());
}

void Moment::add(int am, string spec)
{
  adjust();
  if(spec == "second"){
    tinf->tm_sec += am;
  } else if (spec == "minute"){
    tinf->tm_min += am;
  } else if (spec == "hour"){
    tinf->tm_hour += am;
  } else if (spec == "day"){
    tinf->tm_mday += am;
  } else if (spec == "month"){
    tinf->tm_mon += am;
  } else if (spec == "year"){
    tinf->tm_year += am;
  } else {
  }
  unpack();
}

void Moment::subtract(int am, string spec)
{
  adjust();
  if(spec == "second"){
    tinf->tm_sec -= am;
  } else if (spec == "minute"){
    tinf->tm_min -= am;
  } else if (spec == "hour"){
    tinf->tm_hour -= am;
  } else if (spec == "day"){
    tinf->tm_mday -= am;
  } else if (spec == "month"){
    tinf->tm_mon -= am;
  } else if (spec == "year"){
    tinf->tm_year -= am;
  } else {
  }
  unpack();
}

bool Moment::isBefore(Moment& other)
{
  return toString() < other.toString();
}

bool Moment::isAfter(Moment& other)
{
  return toString() > other.toString();
}

bool Moment::isSame(Moment& other)
{
  return toString() == other.toString();
}

bool Moment::operator<(Moment& other)
{
  return isBefore(other);
}

bool Moment::operator<=(Moment& other)
{
  return isBefore(other) || isSame(other);
}

bool Moment::operator>(Moment& other)
{
  return isAfter(other);
}

bool Moment::operator>=(Moment& other)
{
  return isAfter(other) || isSame(other);
}

bool Moment::operator==(Moment& other)
{
  return isSame(other);
}

bool Moment::operator!=(Moment& other)
{
  return !isSame(other);
}


class Span {
  public: 
    Span(Span& other) {start = other.getStart().toString(); end = other.getEnd().toString(); }
    Span(string start, string end) : start(start), end(end) {}
    string toString(void);
    Moment& getStart(void);
    Moment& getEnd(void);
    bool isSame(Span&);
    bool contains(Span&);
    bool subsumes(Span&);
    bool overlaps(Span&);
  private: 
    Moment start;
    Moment end;
};

string Span::toString(void) 
{
  ostringstream oss;
  oss << start.toString() << " - " << end.toString();
  return oss.str();
}

Moment& Span::getStart(void)
{
  return start;
}

Moment& Span::getEnd(void)
{
  return end;
}

bool Span::isSame(Span& other)
{
  return (start == other.getStart()) && (end == other.getEnd());
}

bool Span::contains(Span& other)
{
  return (start <= other.getStart()) && (end >= other.getEnd());
}

bool Span::subsumes(Span& other)
{
  return (start < other.getStart()) && (end > other.getEnd());
}

bool Span::overlaps(Span& other)
{
  return ((start >= other.getStart()) && (start < other.getEnd())) || ((end > other.getStart()) && (end <= other.getEnd())) || subsumes(other);
}

class Range {
  public:
    Range(string start, string end, int duration, string unit) : start(start), end(end), duration(duration), unit(unit) { skip = duration; generate(); }
    Range(string start, string end, int duration, int skip, string unit) : start(start), end(end), duration(duration), skip(skip), unit(unit) { generate(); }
    void generate(void);
    string toString(void);
    void clear(void);
    void reset(void);
    int size(void){ return store.size(); }
    Span* next(void);
    Span* operator[](int);
  private:
    Moment start;
    Moment end;
    string unit;
    int duration;
    int skip;
    vector<Span*> store;
    vector<Span*>::iterator ip;
};

void Range::generate(void)
{

  Moment anchor(start);
  Moment advance(start);
  Moment en(end);

  clear();

  while(advance < en)
  {

    advance.setstring(anchor.toString());
    advance.add(duration, unit);

    Span* ts = new Span(anchor.toString(), advance.toString());

    store.push_back(ts);

    anchor.add(skip, unit);


  }

  ip = store.begin();

}

string Range::toString(void)
{

  ostringstream oss;

  for(auto&& i : store )
  {
    oss << i->toString() << endl;
  }

  return oss.str();

}

void Range::clear(void)
{
  for(int i = 0; i < store.size(); i++)
  {
    delete store[i];
  }
  store.clear();
}

Span* Range::operator[](int pos)
{
  return store[pos];
}

Span* Range::next(void) 
{
  if(ip == store.end()){
    reset();
    return nullptr;
  } 
  return *ip++;
}

void Range::reset(void)
{
  ip = store.begin();
}
